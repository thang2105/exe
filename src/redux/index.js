import { combineReducers } from "redux";
import {postReducers } from "./reducer";


const reducers = combineReducers({
    posts: postReducers,
    // reducer: reducer
})
export default(state,action) => reducers(state,action)