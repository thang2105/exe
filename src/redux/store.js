import { createStore ,applyMiddleware} from "redux";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";
import reducers from './index'

const middleware = [thunk];
if(process.env.NODE_ENV !== 'production'){
    middleware.push(createLogger())
}
export let store = createStore(reducers,applyMiddleware(...middleware))