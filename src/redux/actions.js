import axios from "axios";

export const ADD_TODO = "ADD_TODO";
export const DELETE_TODO = "DELETE_TODO";
export const UPDATE_TODO = "UPDATE_TODO";

//api
export const AXIOS_POST_REQUEST ="AXIOS_POST_REQUEST";
export const AXIOS_POST_SUCCESS ="AXIOS_POST_SUCCESS";
export const AXIOS_POST_ERROR ="AXIOS_POST_ERROR";

export const loadPost =()=>async dispatch =>{
    try {
        dispatch ({type : AXIOS_POST_REQUEST});
        const url ='https://jsonplaceholder.typicode.com/users';
        const response = await axios.get(url);
        const responseBody = await response.data;
        
        dispatch({
            type: AXIOS_POST_SUCCESS,
            data: responseBody
        })
    }catch(error){
        console.log(error)
        dispatch({
            type: AXIOS_POST_ERROR,
            message: error
        })
    }
}


export function addTodo(datas){
    return{
        type: ADD_TODO,
        payload: datas,
    }
}
export function deleteTodo (datasId){
    return{
        type:DELETE_TODO,
        payload: datasId
    }
}

export function updateTodo(datas) {
    return {
        type:UPDATE_TODO,
        payload: datas,
    }
}