import { ADD_TODO, UPDATE_TODO, DELETE_TODO } from './actions';
// import { todos } from './states';
//api
import {
    AXIOS_POST_ERROR,
    AXIOS_POST_REQUEST,
    AXIOS_POST_SUCCESS
} from './actions'
const initialState = []
export function postReducers(state = initialState, action) {
    let postState = state.data;
    switch (action.type) {
        
        case AXIOS_POST_REQUEST:
            return {
                ...state,
                requesting: true
            }
        case AXIOS_POST_SUCCESS:
            return {
                ...state,
                requesting: false,
                success: true,
                data: action.data
            }
        case AXIOS_POST_ERROR:
            return {
                ...state,
                requesting: false,
                success: false,
                data: action.message
            }     
        case ADD_TODO:     
            postState.unshift(action.payload);
            return {
                data: postState
            }
        case UPDATE_TODO:
        let index = -1;
        for (let i = 0; i < postState.length; i++) {
            index++;
            if (postState[i].id === action.payload.id) {
                break;
            }

        }
        if (index !== -1) {
            postState[index] = action.payload;
            return {
                data : postState
            }
        }; 
        case DELETE_TODO:
            postState =postState.filter(data => data.id !== action.payload)
            return {
                data:postState
            }
        default:
            return state;
    }
}
//

