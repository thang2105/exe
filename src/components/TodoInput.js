import React, { useState } from 'react'
import { addTodo } from '../redux/actions';
import { v4 as uuid } from 'uuid'
import { useDispatch } from 'react-redux';
export default function TodoInput() {
    //add
    const [name, setName] = useState();
    let dispatch = useDispatch()
    return (
        <div>
            <div className="row m-2">
                <input onChange={(e) => setName(e.target.value)}
                    value={name}
                    type="text" className="col form-control" />
                <button className="btn btn-primary mx-2"
                    onClick={() => {
                        dispatch(
                            addTodo({
                                id: uuid(),
                                name: name
                            }))
                        setName('');
                    }}>Add</button>
            </div>
        </div>
    )
}
