import React from 'react'
import TodoItem from './TodoItem'
import { useSelector, useDispatch } from 'react-redux'
import { useEffect } from 'react';
import { loadPost } from '../redux/actions'


export default function TodoList() {
    //connect react với redux ( state reducer)
    let todos = useSelector(state=>state);
    // console.log({todos:todos});
    const datas = useSelector(state => state.posts.data)
    const requesting = useSelector(state => state.posts.requesting)
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(loadPost());
    }, [])
    return (
        <div className="my-4">
            {/* {todos.map((todo)=>   {
                return <TodoItem key={todo.id} todo={todo}/>;
            })} */}
            {
                requesting ?
                    <div>Loading</div>
                    :
                    (datas && datas.length > 0) ? <div>
                        {datas.map((data) => {
                            return <TodoItem key={data.id} datas={data} />;
                        })}
                    </div>
                        :
                        <div>DATA EMPTY</div>
            }
        </div>
    )
}
