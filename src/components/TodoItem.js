import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { deleteTodo, updateTodo } from '../redux/actions'

export default function TodoItem({datas}) {
    const [editable, setEditable] = useState(false)
    const [name, setName] = useState(datas.name)
    let dispatch = useDispatch();

    return (
        <div>
            <div className="row mx-2 align-items-center">
                <div>#{datas.id.length > 1 ? datas.id[2] : datas.id}</div>
                <div className="col">
                    {editable ?
                        <input type="text" className="form-control"
                            placeholder={name}
                            onChange={(e) => {
                                setName(e.target.value);
                            }}

                        />
                        :
                        <h4>{datas.name}</h4>}
                </div>
                <button className="btn btn-primary m-2"
                    onClick={() => {
                        dispatch(updateTodo({
                            ...datas,
                            name: name
                        }))
                        if(editable) {
                         setName(datas.name);   
                        }
                        setEditable(!editable);
                      

                    }}
                >{editable?"Update":"Edit"}</button>
                <button className="btn btn-danger m-2"
                    onClick={() => dispatch(deleteTodo(datas.id))}
                >Delete</button>
            </div>
        </div>
    )
}
